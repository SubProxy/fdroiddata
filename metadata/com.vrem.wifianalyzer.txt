Categories:System,Connectivity
License:Apache2
Web Site:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer/blob/HEAD/README.md
Source Code:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer
Issue Tracker:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer/issues

Auto Name:WiFi Analyzer
Summary:Optimize your WiFi network
Description:
Support Android OS 5.1+

Optimize your WiFi network by examining surrounding WiFi networks, measuring
their signal strength as well as identifying crowded channels.

Features:

* Identify nearby Access Points
* Graph channels signal strength
* Graph Access Point signal strength over time
* Analyze WiFi networks to rate channels
.

Repo Type:git
Repo:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer.git

Build:1.4.2.33,9
    commit=V1.4.1.31
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.4.1.31
Current Version Code:8
